from checkers_class import *
import time
def main():
    if player.color == 'white':
        first = player
        second = cpu
    else:
        first = cpu
        second = player

    while True:
        first.check_lady()
        second.check_lady()

        time.sleep(1)
        game_board.show_board()

        time.sleep(1)
        if  first == player:
            if len(first.capture_scan()) == 0:
                first.move()
            else:
                first.capture()
        else:
            if first.capture() == False:
                first.move()

        if len(second.units) == 0:
            print(f'{first.color.upper()} wins!')
            break

        time.sleep(1)
        game_board.show_board()

        time.sleep(1)
        if second == player:
            if len(second.capture_scan()) == 0:
                second.move()
            else:
                second.capture()
        else:
            if second.capture() == False:
                second.move()
        if len(first.units) == 0:
            print(f'{second.color.upper()} wins!')
            break

if __name__ == '__main__':
    main()
