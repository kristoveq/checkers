import random
import time
from string import ascii_uppercase
from random import choice

letters = list(ascii_uppercase[:8])
colors = ['white', 'black']
white_field = '\U0001F532'
white_pawn = '\u26aa'
white_lady = '\U0001f534'
black_field = '\U0001F533'
black_pawn = '\u26ab'
black_lady = '\U0001F535'

class Board:
    def __init__(self):
        self.size = 8
        self.rows = [num for num in range(1, 8 + 1)]
        self.columns = {}
        for letter, row in zip(letters, self.rows):
            self.columns.setdefault(letter, row)

    def board_initialization(self):
        fields = []
        for col in game_board.columns.items():
            for row in game_board.rows:
                if (col[1] + row) % 2 == 0:
                    appearance = white_field
                else:
                    appearance = black_field
                fields.append(Field({col[0] : row}, appearance, False))
        return fields

    def pawns_initialization(self):
        pawns =[] # 0 = white  ;  1 = black
        white = []
        black = []
        for col in game_board.columns.items():
            for row in game_board.rows[:3]:
                if (col[1] + row) % 2 != 0:
                    appearance = white_pawn
                    white.append(Pawn({col[0] : row}, appearance, 'white',False))
            for row in game_board.rows[5:]:
                if (col[1] + row) % 2 != 0:
                    appearance = black_pawn
                    black.append(Pawn({col[0] : row}, appearance, 'black', False))
        pawns.append(white)
        pawns.append(black)
        return(pawns)

    def pawns_on_board(self):
        all_pawns = []
        for pawn_list in pawns:
            all_pawns.extend(pawn_list)
        for field in fields:
            located = False
            current_look = None
            for empty in empty_fields:
                if field.marker == empty.marker:
                    current_look = empty.appearance
                    located = False
            for pawn in all_pawns:
                if field.marker == pawn.marker:
                    current_look = pawn.appearance
                    located = True
            field.occupied = located
            field.appearance = current_look

    def show_board(self):
        game_board.pawns_on_board()
        print('\t', end=' ')
        for letter in letters:
            print(f'\033[1m{letter}\033[0m', end='  ')
        print()
        for row in range(self.size):
            print(f'\033[1m{row+1}\033[0m', end='\t')
            for col in range(self.size):
                index = col * self.size + row
                field = fields[index].appearance
                print(field, end=' ')
            print()

class Field(Board):
    def __init__(self, marker, appearance, occupied):
        super().__init__()
        self.marker = marker
        self.appearance = appearance
        self.occupied = occupied

class Pawn(Board):
    def __init__(self, marker, appearance, kind, lady):
        self.marker = marker
        self.appearance = appearance
        self.kind = kind
        self.lady = lady

    def locator_by_marker(self, sketch, set):
        for factor in set:
            if sketch == factor.marker:
                result = factor
                return result

    def upper_left(self, pawn):
        for key, value in pawn.marker.items():
            col = key
            row = value
        if col == 'A' or row == 1:
            return False
        else:
            letter_index = letters.index(col)
            digit_index = game_board.rows.index(row)

            if pawn.lady == True:
                new_pos = []
                digit_range = game_board.rows[digit_index - 1:: -1]
                letter_range = 1

                for number in digit_range:
                    new_col = letters[letter_index - letter_range]
                    letter_range += 1
                    new_row = number
                    new_coordinates = {new_col: new_row}
                    check_occupy_field = self.locator_by_marker(new_coordinates, fields)

                    if check_occupy_field.occupied == True:
                        new_pos.append(new_coordinates)
                        break
                    else:
                        new_pos.append(new_coordinates)

                    if new_col == 'A' or new_row == 1:
                        break
            else:
                new_col = letters[letter_index - 1]
                new_row = game_board.rows[digit_index - 1]
                new_pos = {new_col: new_row}
        return new_pos

    def upper_right(self, pawn):
        for key, value in pawn.marker.items():
            col = key
            row = value
        if col == 'H' or row == 1:
            return False
        else:
            letter_index = letters.index(col)
            digit_index = game_board.rows.index(row)

            if pawn.lady == True:
                new_pos = []
                digit_range = game_board.rows[digit_index - 1 :: -1]
                letter_range = 1

                for number in digit_range:
                    new_col = letters[letter_index + letter_range]
                    letter_range += 1
                    new_row = number
                    new_coordinates = {new_col : new_row}
                    check_occupy_field = self.locator_by_marker(new_coordinates, fields)

                    if check_occupy_field.occupied == True:
                        new_pos.append(new_coordinates)
                        break
                    else:
                        new_pos.append(new_coordinates)

                    if new_col == 'H' or new_row == 1:
                        break
            else:
                new_col = letters[letter_index + 1]
                new_row = game_board.rows[digit_index - 1]
                new_pos = {new_col : new_row}
        return new_pos

    def lower_left(self, pawn):
        for key, value in pawn.marker.items():
            col = key
            row = value
        if col == 'A' or row == 8:
            return False
        else:
            letter_index = letters.index(col)
            digit_index = game_board.rows.index(row)

        if pawn.lady == True:
            new_pos = []
            digit_range = game_board.rows[digit_index + 1 :: 1]
            letter_range = 1

            for number in digit_range:
                new_col = letters[letter_index - letter_range]
                letter_range += 1
                new_row = number
                new_coordinates = {new_col: new_row}
                check_occupy_field = self.locator_by_marker(new_coordinates, fields)

                if check_occupy_field.occupied == True:
                    new_pos.append(new_coordinates)
                    break
                else:
                    new_pos.append(new_coordinates)

                if new_col == 'A' or new_row == 8:
                    break
        else:
            new_col = letters[letter_index - 1]
            new_row = game_board.rows[digit_index + 1]
            new_pos = {new_col: new_row}
        return new_pos

    def lower_right(self, pawn):
        for key, value in pawn.marker.items():
            col = key
            row = value
        if col == 'H' or row == 8:
            return False
        else:
            letter_index = letters.index(col)
            digit_index = game_board.rows.index(row)

            if pawn.lady == True:
                new_pos = []
                digit_range = game_board.rows[digit_index + 1:: 1]
                letter_range = 1

                for number in digit_range:
                    new_col = letters[letter_index + letter_range]
                    letter_range += 1
                    new_row = number
                    new_coordinates = {new_col: new_row}
                    check_occupy_field = self.locator_by_marker(new_coordinates, fields)

                    if check_occupy_field.occupied == True:
                        new_pos.append(new_coordinates)
                        break
                    else:
                        new_pos.append(new_coordinates)

                    if new_col == 'H' or new_row == 8:
                        break
            else:
                new_col = letters[letter_index + 1]
                new_row = game_board.rows[digit_index + 1]
                new_pos = {new_col: new_row}
        return new_pos

class Player(Pawn):
    def __init__(self):
        color = choice(colors)
        self.color = color
        if self.color == 'white':
            self.units = pawns[0]
            print('You play WHITE.')
        else:
            self.units = pawns[1]
            print('You play BLACK.')

    def check_lady(self):
        for pawn in self.units:
            if pawn.kind == 'white' and pawn.lady == False:
                if 8 in pawn.marker.values():
                    pawn.lady = True
                    pawn.appearance = white_lady
                    print(f'Unit in {pawn.marker} become lady {white_pawn} --> {white_lady}')
            else:
                if 1 in pawn.marker.values() and pawn.lady == False:
                    pawn.lady = True
                    pawn.appearance = black_lady
                    print(f'Unit in {pawn.marker} become lady {black_pawn} --> {black_lady}')

    def lady_capturing(self):
        my_units = []
        for unit in self.units:
            my_units.append(unit.appearance)
        all_pawns = [x for one_list in (pawns[0], pawns[1]) for x in one_list]
        ladies = []
        directions = [self.upper_right, self.upper_left, self.lower_right, self.lower_left]
        result = []
        for unit in self.units:
            if unit.lady == True:
                ladies.append(unit)
        for lady in ladies:
            for direct in directions:
                if direct(lady) != False:
                    possible_capture = direct(lady)[-1]
                    possible_field = self.locator_by_marker(possible_capture, fields)
                    if possible_field.occupied == True:
                        possible_pawn = self.locator_by_marker(possible_field.marker, all_pawns)
                        next_loc = direct(possible_pawn)
                        next_field = self.locator_by_marker(next_loc, fields)
                        if (not possible_pawn.appearance in my_units) and next_field is not None and next_field.occupied == False:
                            result.append([lady, possible_pawn, next_field])
        return result

    def move_range(self, pawn):
        possibilities = []
        if pawn.lady == True:
            directions = [self.upper_right, self.upper_left, self.lower_right, self.lower_left]
            temp = []
            for direct in directions:
                if direct(pawn) != False:
                    temp.append(direct(pawn))
            for one_list in temp:
                possibilities.extend(one_list)
        else:
            if self.color == 'white':
                possibilities.append(self.lower_right(pawn))
                possibilities.append(self.lower_left(pawn))
            else:
                possibilities.append(self.upper_right(pawn))
                possibilities.append(self.upper_left(pawn))
        return possibilities

    def move(self):
        if self == player:
            while True:
                player_start = input('Enter coordinates of pawn to move:\t')
                start_pos = {}
                if len(player_start) == 2 and (player_start[0].isalpha() and player_start[0].upper() in letters)\
                                          and (player_start[1].isdigit() and int(player_start[1]) in game_board.rows):
                    start_pos.setdefault(player_start[0].upper(), int(player_start[1]))
                    unit = self.locator_by_marker(start_pos, self.units)
                else:
                    print('Wrong start coordinates!')
                    continue

                player_stop = input('Enter coordinates of the field you want to go to:\t')
                stop_pos = {}
                if len(player_stop) == 2 and (player_start[0].isalpha() and player_start[0].upper() in letters)\
                                          and (player_start[1].isdigit() and int(player_start[1]) in game_board.rows):
                    stop_pos.setdefault(player_stop[0].upper(), int(player_stop[1]))
                    field = self.locator_by_marker(stop_pos, fields)
                else:
                    print('Wrong stop coordinates!')
                    continue

                if not unit in self.units:
                    print('Wrong start coordinates!')
                    continue
                if (not field in fields) or field.occupied == True or not field.marker in self.move_range(unit):
                    print('Wrong stop coordinates!')
                    continue
                else:
                    old_field = self.locator_by_marker(unit.marker, fields)
                    unit.marker = field.marker
                    old_field.occupied = False
                    break
        else:
            walkers = []
            for unit in self.units:
                for field in fields:
                    if field.occupied == False and field.marker in self.move_range(unit):
                        walkers.append([unit, field])

            walker = random.choice(walkers)
            pawn = walker[0]
            place = walker[1]
            old_field = self.locator_by_marker(pawn.marker, fields)
            print('Thinking...')
            time.sleep(1)
            print(f'CPU moved from {old_field.marker} to {place.marker}.')
            pawn.marker = place.marker
            place.occupied = True
            old_field.occupied = False

    def capture_scan(self):
        result = []
        result.extend(self.lady_capturing())
        all_pawns = [x for one_list in (pawns[0], pawns[1]) for x in one_list]
        directions = [self.upper_right, self.upper_left, self.lower_right, self.lower_left]
        my_units = []
        for unit in self.units:
            my_units.append(unit.appearance)
        for unit in self.units:
            for direct in directions:
                if direct(unit) != False:
                    possible_pos = direct(unit)
                    for pawn in all_pawns:
                        if pawn.marker == possible_pos:
                            possible_pawn = pawn
                            if not possible_pawn.appearance in my_units:
                                battle = True
                                if direct(possible_pawn) != False:
                                    new_pos = direct(possible_pawn)
                                    if len(new_pos) > 1:
                                        new_pos = new_pos[0]

                                    for field in fields:
                                        if field.marker == new_pos:
                                            new_field = field
                                            if new_field.occupied == False:
                                                permission = True
                                                if battle == True and permission == True:
                                                    result.append([unit, possible_pawn, new_field])
        return result

    def capture(self):
        base = self.capture_scan()
        if len(base) > 0:
            if self == player:
                while True:

                    print('You have to capture!')
                    for factor in base:
                        print(f'{factor[0].appearance} {factor[0].marker} can beat {factor[1].appearance} {factor[1].marker}.')
                    choice = input('Enter coordinates of your beating pawn : ')
                    capture = {choice[0].upper() : int(choice[1])}

                    for factor in base:
                        if capture == factor[0].marker:
                            factor[0].marker = factor[2].marker
                            cpu.units.remove(factor[1])
                    else:
                        return False

            else:
                factor = random.choice(base)
                print('CPU has to capture!')
                print(f'{factor[0].appearance} {factor[0].marker} beating {factor[1].appearance} {factor[1].marker}.')
                factor[0].marker = factor[2].marker
                player.units.remove(factor[1])
                return True
        else:
            return False

class CPU(Player):
    def __init__(self):
        if player.color == 'white':
            self.color = 'black'
            self.units = pawns[1]
        else:
            self.color = 'white'
            self.units = pawns[0]

game_board = Board()
fields = game_board.board_initialization()
empty_fields = game_board.board_initialization()
pawns = game_board.pawns_initialization()
player = Player()
cpu = CPU()